///////////////// LIBRERIAS

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include <assert.h>

/////////////////


///////////////// HEADERS

#include "GList.h"
#include "persona_libreria.h"

/////////////////


///////////////// SIGNATURAS
/**
extraer_palabras: char*, char*, int*, char*
Recibe como parametro una linea leida del archivo de la forma (nombre,edad,lugarDeNacimiento)
dos string y un puntero a int. Se encarga de separar la linea y guardar cada dato en su
matriz correspondiente. Finalmente, guarda el int correspondiente a la edad en el puntero
a int recibido como parametro
*/
void extraer_palabras (char* buffer, char* nombre, int* edad, char* pais);

/**
parser: char*, char**, int*, char**
Se encarga de reservar la memoria adecuada para almacenar los datos leidos
del archivo. Finalmente los guarda en su matriz correspondiente.
*/
void parser (char* buffer, char** nombre, int* edad, char** pais);

/**
agregar_persona: char*, GList -> GList
Recibe como parametro una linea leida de un archivo y una lista. Se encarga
de crear la persona con los datos recibidos y agregarla a nuestra lista.
*/
GList agregar_persona (char* buffer, GList inicio);

/**
lectura_valida: char* -> int
Recibe como parametro una ruta de un archivo, checkea que el archivo exista y que no este vacio.
Si verifica las condiciones devuelve 1, en caso contrario, devuelve 0.
*/
int lectura_valida (char* rutaArchivoPersonas);

/**
leer_archivo: char*, GList, int* -> GList
Recibe como parametro la ruta de un archivo, una lista y una bandera. Si la ruta es valida, lee el archivo
y por cada linea se encarga de agregar la persona a la lista, finalmente, guarda un 1 en la bandera y
devuelve la lista. En caso que la ruta no sea valida, devuelve NULL y no modifica la bandera.o
*/
GList leer_archivo (char* rutaArchivoPersonas, GList inicio, int* lecturaValida);

/**
interfaz: Char**, Char**, int, int
Tiene como objetivo guiar al usuario para que sea mas facil el uso de las funciones.
Ademas se encarga del manejo de la memoria
*/
void interfaz (GList inicio, GList mapeo, GList filtrado);

/////////////////


///////////////// AUXILIARES

void extraer_palabras (char* buffer, char* nombre, int* edad, char* pais) {
    char edadAux[4] = {-1};
    int i = 0, j;
    for (j = 0; buffer[i] != ','; i++, j++) {
        nombre[j] = buffer[i];
    }
    nombre[j] = '\0';
    i++;

    for (j = 0; buffer[i] != ','; i++, j++) {
        edadAux[j] = buffer[i];
    }
    edadAux[j] = '\0';
    i++;

    for (j = 0; buffer[i] != '\n' && buffer[i] != '\r'; i++, j++) {
        pais[j] = buffer[i];
    }
    pais[j] = '\0';

    assert (edadAux[0] != '\0');
    *edad = atoi(edadAux);

}

void parser (char* buffer, char** nombre, int* edad, char** pais) {
    int largoNombre = 0, largoPais = 0;
    char nombreAux[50], paisAux[50];

    extraer_palabras(buffer, nombreAux, edad, paisAux);
    largoNombre = strlen(nombreAux);
    largoPais = strlen(paisAux);

    assert(largoNombre > 0);
    assert(largoPais > 0);

    *nombre = malloc(sizeof(char) * (largoNombre + 1));
    *pais = malloc(sizeof(char) * (largoPais + 1));

    strcpy(*nombre, nombreAux);
    strcpy(*pais, paisAux);

}

GList agregar_persona (char* buffer, GList inicio) {
    Persona* personaNueva;
    char *nombre, *pais;
    int edad = 0;
    parser(buffer, &nombre, &edad, &pais);
    personaNueva = crear_Persona(nombre, edad, pais);
    inicio = GList_agregar_final(inicio, personaNueva);
    return inicio;
}

/////////////////


///////////////// VALIDACION

int lectura_valida (char* rutaArchivoPersonas) {
    char aux;
    FILE* archivo;
    archivo = fopen(rutaArchivoPersonas,"r");
    if (archivo != NULL) {
        aux = fgetc (archivo);
        if (aux != EOF) {
            fclose(archivo);
            return 1;
        }
    }
    return 0;
}

/////////////////

///////////////// ENTRADA Y SALIDA

GList leer_archivo (char* rutaArchivoPersonas, GList inicio, int* lecturaValida) {

    if (lectura_valida(rutaArchivoPersonas)) {
        FILE* archivo;
        char buffer[100];
        archivo = fopen(rutaArchivoPersonas,"r");

        while (!feof(archivo)) {
            fgets(buffer,100,archivo);

            inicio = agregar_persona(buffer, inicio);
        }

        fclose(archivo);
        *lecturaValida = 1;
        return inicio;
    }
    return NULL;
}

/////////////////


///////////////// INTERFAZ

void interfaz (GList inicio, GList mapeo, GList filtrado ) {
    int opcion = -1, opcionMap, opcionFilter, lecturaValida = 0;
    char rutaArchivoPersonas[50];
    while (!lecturaValida) {
        printf("Ingrese la ruta del archivo de personas: (Si esta en el mismo directoria que el ejecutable, solo ingrese el archivo con su extension)\n\n");
        scanf("%s",rutaArchivoPersonas);
        inicio = leer_archivo(rutaArchivoPersonas,inicio, &lecturaValida);
    }
    while (opcion != 0) {
        printf("\nQue desea hacer?\n0. Finalizar el programa\n1. Aplicar map\n2. Aplicar filter\n");
        scanf("%d",&opcion);
        switch (opcion) {
            case 1:{
                printf("\nQue funcion desea mapear?\n1. Poner nombre en mayuscula\n2. Avanzar un anio\n3. Edad al reves\n4. Viven todos en Rosario\n");
                scanf("%d",&opcionMap);
                switch (opcionMap) {
                    case 1: {
                        mapeo = map(inicio, &poner_nombre_mayuscula, &copiar_persona);
                        GList_imprimir(mapeo, "NombresEnMayuscula.txt", &imprimir_persona);
                        GList_destruir(mapeo, &destruir_Persona);
                        printf("\nSe creo el archivo 'NombresEnMayuscula.txt' con el resultado\n");
                        break;
                    }
                    case 2: {
                        mapeo = map(inicio, &avanzar_un_year, &copiar_persona);
                        GList_imprimir(mapeo, "AvanzoUnAnio.txt", &imprimir_persona);
                        GList_destruir(mapeo, &destruir_Persona);
                        printf("\nSe creo el archivo 'AvanzoUnAnio.txt' con el resultado\n");
                        break;
                    }

                    case 3: {
                        mapeo = map(inicio, &edad_al_reves, &copiar_persona);
                        GList_imprimir(mapeo, "EdadAlReves.txt", &imprimir_persona);
                        GList_destruir(mapeo, &destruir_Persona);
                        printf("\nSe creo el archivo 'EdadAlReves.txt' con el resultado\n");
                        break;
                    }
                    case 4: {
                        mapeo = map(inicio, &ahora_viven_en_rosario, &copiar_persona);
                        GList_imprimir(mapeo, "AhoraVivenEnRosario.txt", &imprimir_persona);
                        GList_destruir(mapeo, &destruir_Persona);
                        printf("\n Secreo el archivo 'AhoraVivenEnRosario.txt' con el resultado\n");
                        break;
                    }
                    default:
                        printf("\nEl valor ingresado no es valido\n");
                }
                break;
            }
            case 2: {
            printf("\nBajo que predicado desea filtrar?\n1. Mayores de edad\n2. Nombre comienza con 'A'\n3. Vive en Argentina\n4. Su nombre tiene todas las vocales \n");
            scanf("%d",&opcionFilter);
            switch (opcionFilter) {
                    case 1:{
                        filtrado = filter(inicio, &es_mayor, &copiar_persona);;
                        GList_imprimir(filtrado, "MayoresDe18.txt", &imprimir_persona);
                        GList_destruir(filtrado, &destruir_Persona);
                        printf("\nSe creo el archivo 'MayoresDe18.txt' con el resultado\n");
                        break;
                    }
                    case 2: {
                        filtrado = filter(inicio, &empieza_con_a, &copiar_persona);
                        GList_imprimir(filtrado, "NombresEmpiezanConA.txt", &imprimir_persona);
                        GList_destruir(filtrado, &destruir_Persona);
                        printf("\nSe creo el archivo 'NombresEmpiezanConA.txt' con el resultado\n");
                        break;
                    }
                    case 3: {
                        filtrado = filter(inicio, &vive_en_Argentina, &copiar_persona);
                        GList_imprimir(filtrado, "VivenEnArgentina.txt", &imprimir_persona);
                        GList_destruir(filtrado, &destruir_Persona);
                        printf("\nSe creo el archivo 'VivenEnArgentina.txt' con el resultado\n");
                        break;
                    }
                    case 4: {
                        filtrado = filter(inicio, &contiene_todas_las_vocales_en_nombre, &copiar_persona);
                        GList_imprimir(filtrado, "NombresConTodasLasVocales.txt", &imprimir_persona);
                        GList_destruir(filtrado, &destruir_Persona);
                        printf("\nSe creo el archivo 'NombresConTodasLasVocales.txt' con el resultado\n");
                        break;
                    }
                   default:
                        printf("\nEl valor ingresado no es valido\n");
                }
                break;
            }
            case 0:{
                printf("\nFinalizo el programa\n");
                opcion = 0;
                GList_destruir(inicio, &destruir_Persona);
                break;
            }
            default:
                printf("\nEl valor ingresado no es valido\n");
        }
    }
}

/////////////////


///////////////// PRINCIPAL

int main() {
    GList inicio, mapeo, filtrado;
    inicio = GList_crear();


    interfaz(inicio, mapeo, filtrado);

    return 0;
}

/////////////////
